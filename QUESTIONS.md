# Questions

Q1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

The callback parameter `setTimeout` is supposed to be asynchronously called (in this case after `100ms` from being registered) and is subsequently added to a queue. In the meanwhile all the synchronous code has to run first (i.e. on the main stack) and this would lead to `console.log("2")` executing first and displaying `2` in the console. About `100ms` afterwards we should see "1" appear in the console when the callback is executed (i.e. `console.log("1")`).

Q2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

We start by calling our function `foo` with `d = 0`. And this will lead to recursive calls until `d >= 10` (i.e. `d = 10`). As the last recursive call will be with `d = 10` this would run `console.log(d)` such as `d = 10`. and then the upper call on the stack (`d = 9`) will be able call its `console.log` and so on leading up (down) to `0`. We will hence get the numbers going from `10` to `0` displayed successively on the console.

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

if `d` is `undefined` (or any falsy value for that matter) this will lead to `d = 5`. The problem is for example with boolean values where `d` is equal to `false` and where the intended value is `false`. This will lead to the value being changed to `5` even though we only wanted a default value (i.e. when the value is `undefined`). This problem will be encountered with the following values which would evaluate to `false` in this context: `-0, 0n, -0n, "", '', ``, null, NaN, document.all` ([source](https://developer.mozilla.org/en-US/docs/Glossary/Falsy)).

Q4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

`foo` is a higher order function that returns another function with the value of `a` set in it's parameter. In our case it will be set to `1`. In principal we just created a function that tries to increment any value that is passed to it (called `bar` in our context). `console.log(bar(2))` will lead to the operation `a + b` evaluating with the values `a = 1` and `b = 2` leading to the result `3` logged on the console.

Q5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

The `double` function will call `done` as a callback function with the value of `a` multiplied by `2` after about a `100ms` timeout.