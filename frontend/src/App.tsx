import React, { useEffect, useState } from 'react';

import './App.css';

import SearchBar from './components/SearchBar';

import { search, SearchResult } from './api/search';
import SearchResults from './components/SearchResults';

function App() {
  const [input, setInput] = useState('');
  const [searchResults, setSearchResults] = useState<SearchResult[]>([]);

  useEffect(() => {
    search(input).then(
      (result) => setSearchResults(result.data)
    );
  }, [input]);

  return (
    <div className="App">
      <SearchBar
        input={input}
        setInput={setInput}
      />
      <SearchResults
        results={searchResults}
      />
    </div>
  );
}

export default App;
