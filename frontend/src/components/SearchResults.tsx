import { SearchResult } from '../api/search';

type SearchProps = {
  results: SearchResult[];
}

const SearchResults = (props: SearchProps) => {
  const { results } = props;

  const SearchResultsStyle = {
    width: '20rem',
    background: '#fafafa',
    border: 'none',
    padding: '0.5rem',
    margin: '2rem auto 0',
    borderRadius: '0.5rem',
    listStyleType: 'none',
  };

  const SearchResultStyle = {
    display: 'flex',
    justifyContent: 'space-between'
  };

  return results.length ? (
    <ul
     style={SearchResultsStyle}
    >
      {results.map(result =>
        <li
          key={result.id}
          style={SearchResultStyle}
        >
          <span>{result.name}</span>
          <span>({result.latitude}, {result.longitude})</span>
        </li>)
      }
    </ul>
  ) : <></>;
}

export default SearchResults