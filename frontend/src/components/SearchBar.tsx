import React from 'react';

type SearchProps = {
  input: string;
  setInput: (value: string) => void;
}

const SearchBar = (props: SearchProps) => {
  const { input, setInput } = props;

  const BarStyling = {
    width: '20rem',
    background: '#f5f5f5',
    border: 'none',
    padding: '0.5rem',
    marginTop: '5rem',
    borderRadius: '0.5rem'
  };

  return (
    <input 
     style={BarStyling}
     value={input}
     placeholder={"Search points of interest in the UK"}
     onChange={(e) => setInput(e.target.value)}
    />
  );
}

export default SearchBar