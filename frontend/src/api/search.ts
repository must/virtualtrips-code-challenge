import axios from 'axios';


export type SearchResult = {
  id: string;
  name: string;
  alternateName: string;
  latitude: string;
  longitude: string;
};

export const search = async (q: string) => {
  return axios.get<SearchResult[]>('http://localhost:8080/locations', {
    params: { q }
  })
};
