import { Application, Router } from 'express';
import { LocationsController } from './controllers/LocationsController';

const _routes: [string, Router][] = [
  ['/', LocationsController],
];

export const routes = (app: Application) => {
  _routes.forEach((route) => {
    const [url, controller] = route;
    app.use(url, controller);
  });
};
