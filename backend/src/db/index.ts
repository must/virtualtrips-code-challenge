import sqlite3 from 'sqlite3';

export type LocationRecord = {
  id: string;
  name: string;
  alternatenames: string;
  latitude: string;
  longitude: string;
}

export class DB {
  private db;
  constructor() {
    this.db = new (sqlite3.verbose()).Database(':memory:');
  }

  initialize(records: LocationRecord[]) {
    const ldb = this.db;

    ldb.serialize(function() {
      ldb.run(`
        CREATE TABLE locations (
          id PRIMARY KEY,
          name TEXT,
          alternatenames TEXT NULL,
          latitude TEXT,
          longitude TEXT
        )
      `);

      const stmt = ldb.prepare(`
        INSERT INTO locations (id, name, alternatenames, latitude, longitude)
        VALUES (?, ?, ?, ?, ?)
      `);

      for(const record of records) {
        stmt.run(
          record.id,
          record.name,
          record.alternatenames ?? null,
          record.latitude,
          record.longitude
        );
      }

      stmt.finalize();
    });
  }

  async suggestByName(name: string) {
    return new Promise((resolve, reject) => {
      const stmt = this.db.prepare(`
        SELECT id, name, alternatenames, latitude, longitude
        FROM locations
        WHERE name LIKE ? ESCAPE '!'
      `);

      name = name
        .replace('!', '!!')
        .replace('%', '!%')
        .replace('_', '!_')
        .replace('[', '![');
      name += '%';

      stmt.all(name, function(err, rows) {
        if(err) {
          reject(err);

          return;
        }

        resolve(rows);
      });

      stmt.finalize();
    });
  }

  close() {
    this.db.close();
  }
}
