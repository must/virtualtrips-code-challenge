import { NextFunction, Request, Response, Router } from 'express';
import { LocationLoader } from '../loader/LocationLoader';


export const LocationsController: Router = Router();

const locationLoader = new LocationLoader();
LocationsController.get('/locations', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = req.query.q as string;
    if(! query || query.length < 2) {
      res.json([]);

      return;
    }

    res.json(
      await locationLoader.suggestByName(query)
    );
  } catch(e) {
    next(e);
  }
});
