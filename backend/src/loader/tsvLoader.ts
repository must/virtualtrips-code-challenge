import csv from 'csv-parser';
import fs from 'fs';

type keys = 'geonameid' | 'name' | 'asciiname' |
  'alternatenames' | 'latitude' | 'longitude' |
  'feature_class' | 'feature_code' | 'country_code' |
  'cc2' | 'admin1_code' | 'admin2_code' | 'admin3_code' |
  'admin4_code' | 'population' | 'elevation' | 'dem' |
  'timezone' | 'modification_date';

export type Row = {
  [K in keys]: string;
};

export const load = async () => {
  return new Promise<Row[]>((resolve, reject) => {
    const results: Row[] = [];
    fs.createReadStream('../data/GB.tsv')
      .pipe(csv({
        separator: '\t'
      }))
      .on('data', (row: Row) => {
        results.push(row);
      })
      .on('end', () => {
        resolve(results);
      })
      .on('error', (err) => {
        reject(err);
      });
  });
};
