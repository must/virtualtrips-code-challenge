import { load } from './tsvLoader';
import { DB, LocationRecord } from '../db';


export class LocationLoader {
  private static alreadyLoaded = false;
  private static database: Promise<DB> = null;

  constructor() {
    LocationLoader.database = new Promise((resolve, reject) => {
      load().then((locations) => {
        const db = new DB();
        db.initialize(
          locations.map(
            location => ({
              id: location.geonameid,
              name: location.name,
              alternatenames: location.alternatenames,
              latitude: location.latitude,
              longitude: location.longitude,
            } as LocationRecord)
          )
        );

        resolve(db);
      }).catch((err) => reject(err));
    });
  }

  async suggestByName(query: string) {
    return (await LocationLoader.database).suggestByName(query);
  }
}
