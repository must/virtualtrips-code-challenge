# Virtualtrips Geosearch backend

This is the backend project for the Virtualtrips Geosearch code challenge.

## Installing dependencies
Make sure to install the dependencies of this project by running the following command.
```bash
npm install
```

## Running the project
Once the dependencies are installed, you can run the backend with the following command:
```bash
npm start
```

If you want to make changes (i.e. develop on this project), you might want to run the following command which will rebuild and update the server to serve your latest changes.
```bash
npm run dev
```

## Running tests
To run tests, you should run the following command.
```bash
npm run test
```

Or if you want to watch for test results as you make changes in code.
```bash
npm run test:watch
```

## Project configuration
If you want to customize the port on which the server is served, you can just specify it in an environment variable `SERVER_PORT`. For example through a `.env` file. In that case, you, might want to copy the example file provided.
```bash
cp .env.example .env
```