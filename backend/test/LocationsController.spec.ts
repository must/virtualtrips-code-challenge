/* eslint-disable no-unused-expressions */
import {expect} from 'chai';
import {describe, it} from 'mocha';

import request from 'supertest';
import { app } from '../src/app';

describe('Locations', () => {
  it('Should not return results when the query is less than 2 characters long', async () => {
    await request(app)
      .get('/locations?q=t')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        expect(res.body).to.eql([]);
      });
  });

  it('Should not return results when the query is empty', async () => {
    await request(app)
      .get('/locations?q=')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        expect(res.body).to.eql([]);
      });
  });

  it('Should not return the specific record for Pentahotel Warrington', async () => {
    await request(app)
      .get('/locations?q=Pentahotel Warrington')
      .expect('Content-Type', /json/)
      .expect(function(res) {
        expect(res.body).to.eql([{
          "alternatenames": "",
          "id": "9259725",
          "latitude": "53.42307",
          "longitude": "-2.51987",
          "name": "Pentahotel Warrington",
        }]);
      });
  });
});
